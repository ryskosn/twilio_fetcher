package main

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	twilio "github.com/saintpete/twilio-go"
)

func fetchCalls() twilio.CallPageIterator {
	sid := os.Getenv("TWILIO_ACCOUNT_SID")
	token := os.Getenv("TWILIO_AUTH_TOKEN")
	c := twilio.NewClient(sid, token, nil)

	now := time.Now()
	start := now.AddDate(0, 0, -1)
	return c.Calls.GetCallsInRange(start, now, url.Values{})
}

func showCalls(iter twilio.CallPageIterator) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	for {
		page, err := iter.Next(ctx)
		if err == twilio.NoMoreResults {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		for i, call := range page.Calls {
			t := jst(call.DateCreated.Time)
			fmt.Printf("%d: %s (%s)\n", i, call.To, t)
		}
	}
}

func fetchRecordings() (*twilio.RecordingPage, error) {
	sid := os.Getenv("TWILIO_ACCOUNT_SID")
	token := os.Getenv("TWILIO_AUTH_TOKEN")
	c := twilio.NewClient(sid, token, nil)
	ctx := context.Background()
	return c.Recordings.GetPage(ctx, url.Values{})
}

func showRecordings(page *twilio.RecordingPage) {
	for i, rec := range page.Recordings {
		t := jst(rec.DateCreated.Time)
		fmt.Printf("%d: %s (%s) %s\n", i, t, rec.URL(".wav"), rec.Duration.String())
	}
}

func main2() {
	fmt.Printf("sid: %v\n", os.Getenv("TWILIO_ACCOUNT_SID"))
	fmt.Printf("token: %v\n", os.Getenv("TWILIO_AUTH_TOKEN"))

	// iter := fetchCalls()
	// showCalls(iter)

	iter, err := fetchRecordings()
	if err != nil {
		log.Println(err)
	}
	showRecordings(iter)
}
