package main

import "time"

func jst(t time.Time) time.Time {
	jst := time.FixedZone("Asia/Tokyo", 9*60*60)
	return t.In(jst)
}
