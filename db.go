package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"
	twilio "github.com/saintpete/twilio-go"
)

var (
	db  *sql.DB
	err error
)

func createTables() {
	query := `
    CREATE TABLE IF NOT EXISTS
    calls (
           sid          TEXT PRIMARY KEY
         , created_at   TIMESTAMP
         , to_          TEXT
         , from_        TEXT
          )
    ;`
	if _, err := db.Exec(query); err != nil {
		log.Fatalf("Error creating database table calls: %q", err)
	}
	query2 := `
    CREATE TABLE IF NOT EXISTS
    recs (
          sid          TEXT PRIMARY KEY
        , created_at   TIMESTAMP
        , call_sid     TEXT
        , wav_url      TEXT
        , mp3_url      TEXT
        , duration     TEXT
        , transcribed  BOOLEAN DEFAULT false
        , posted_slack BOOLEAN DEFAULT false
         )
    ;`
	if _, err := db.Exec(query2); err != nil {
		log.Fatalf("Error creating database table recs: %q", err)
	}

}

func insertCall(call *twilio.Call) {
	t := jst(call.DateCreated.Time)
	fmt.Printf("'%s', '%s', '%s', '%s'\n", call.Sid, t.Format(time.RFC3339), call.To, call.From)

	query := `
    INSERT INTO
    calls (sid, created_at, to_, from_)
    SELECT
        '%[1]s', '%[2]s', '%[3]s', '%[4]s'
    WHERE
        NOT EXISTS(
            SELECT
                'x'
            FROM
                calls
            WHERE
                sid = '%[1]s'
        )
    ;`
	query = fmt.Sprintf(query, call.Sid, t.Format(time.RFC3339), call.To, call.From)
	if _, err := db.Exec(query); err != nil {
		log.Printf("failed to insert call: %v\n", err)
	}
}

func insertCalls() {
	iter := fetchCalls()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	for {
		page, err := iter.Next(ctx)
		if err == twilio.NoMoreResults {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		for _, call := range page.Calls {
			insertCall(call)
		}
	}
}

func insertRec(rec *twilio.Recording) {
	t := jst(rec.DateCreated.Time)
	fmt.Printf("'%s', '%s', '%s', '%s'\n", rec.Sid, t.Format(time.RFC3339), rec.CallSid, rec.Duration.String())

	query := `
    INSERT INTO
    recs (sid, created_at, call_sid, wav_url, mp3_url, duration)
    SELECT
        '%[1]s', '%[2]s', '%[3]s', '%[4]s', '%[5]s', '%[6]s'
    WHERE
        NOT EXISTS(
            SELECT
                'x'
            FROM
                recs
            WHERE
                sid = '%[1]s'
        )
    ;`
	query = fmt.Sprintf(query, rec.Sid, t.Format(time.RFC3339), rec.CallSid, rec.URL(".wav"), rec.URL(".mp3"), rec.Duration.String())
	if _, err := db.Exec(query); err != nil {
		log.Printf("failed to insert rec: %v\n", err)
	}
}

func insertRecs() {
	iter, err := fetchRecordings()
	if err != nil {
		log.Println(err)
	}
	for _, rec := range iter.Recordings {
		insertRec(rec)
	}
}

// Rec is used by transcribe service
type Rec struct {
	sid       string `db:"sid"`
	createdAt string `db:"created_at"`
	wavURL    string `db:"wav_url"`
	mp3URL    string `db:"mp3_url"`
	to        string `db:"to_"`
	duration  string `db:"duration"`
}

func readDB() {
	query := `
    SELECT
        recs.sid, recs.created_at, recs.wav_url, recs.mp3_url, calls.to_, recs.duration
    FROM
        recs
    JOIN
        calls
    ON
        recs.call_sid = calls.sid
    WHERE
        recs.transcribed = false
    ORDER BY recs.created_at DESC
    LIMIT 10
    ;`

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var rec Rec
		rows.Scan(&rec.sid, &rec.createdAt, &rec.wavURL, &rec.mp3URL, &rec.to, &rec.duration)
		fmt.Printf("%#v\n", rec)
	}
}

func main() {
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Error opening database: %v", err)
	}
	defer db.Close()
	// createTables()
	// insertCalls()
	// insertRecs()
	readDB()
}
